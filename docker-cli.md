# Docker client installation

This is how you can install the docker client in WSL2. 

Download the latest tar file from 

https://download.docker.com/linux/static/stable/x86_64/docker-20.10.9.tgz

Unpack the tar file and copy the docker/docker file to 
a location in your path, e.g. /usr/local/bin.

```
tar -xzf docker-20.10.9.tgz
sudo copy docker/docker /usr/local/bin
```
