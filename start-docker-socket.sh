#!/bin/bash

sudo -- podman system service --time=0 unix:///var/run/docker.sock &

sudo -- chmod 777 /var/run/docker.sock

# Start the tunnel...
# minikube tunnel