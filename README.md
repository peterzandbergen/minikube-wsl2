# Minikube - podman - wsl2

This tutorial is for people that are interested in using 
Kubernetes on a Windows 10+ PC or laptop. At the end of this post
you should have a workable platform for developing for 
or exploring Kubernetes.

*This tutorial assumes that you are familiar with WSL2 
and Linux commands for editing text files, extracting 
tar files and know how to use sudo.*

## Kubernetes and containers

I experiment a lot with Kubernetes, for work but also out of 
personal interest. Using Kubernetes for the deployment of 
service based applications has a lot of advantages. I think
that any DevOps team member should at least know the 
basics of Kubernetes. My personal experience is that the best way to learn about Kubernetes is by using it. 
For that you need easy access to a Kubernetes cluster.

I use Windows for my default environment because, well, because it is company policy. 
But for serious workloads I prefer Linux, containers 
and Kubernetes. 

## podman on WSL2

My most easily accessible Linux environment is WSL2 with
 Ubuntu. 
I use podman build and run containers. 
Podman's advantage over Docker for Windows, 
is that it does not need a VM to run in and thus consumes less 
memory resources. And it is free.

In the past I used to use Docker for Windows from WSL2 and Kubernetes in Kind, 
but for serious applications with multiple pods the memory consumption was always a problem. 
On more than one occasion, my laptop, a Lenovo Thinkpad P1 Intel i7 with 16GB RAM, would run out of memory. 

This should be no surprise, because the 16GB RAM needs to cater memory for three machines, the Windows OS on bare metal, Ubuntu on the WSL2 VM and Docker with Kubernetes in Kind in the Docker VM. 
Running e.g. NiFi or another Java based application in 
Kind usually meant that all 
memory would be consumed and the Windows OS started swapping and the laptop became unusable.

## **minikube** in podman

One way to lower the memory consumption when running Kubernetes is to get rid of the 
VM with Docker for Windows, and run a Kubernetes cluster in podman containers in the WSL2 VM. 
Minikube offers this option, and although still 
[experimental](https://minikube.sigs.k8s.io/docs/drivers/podman/#experimental), 
it works quite well on WSL2.

I will now take you through the steps I followed to get Minikube using podman as target and have 
a workable Kubernetes environment.

## <a name="preparation"></a>Preparation

First you need to make sure that you have podman working on WSL2. And you need to install some additional tools
to work with Kubernetes. 

You find the links for the tools you need to install below. 

- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [podman](https://podman.io/getting-started/installation)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [docker client](docker-cli.md)

## Starting Minikube with podman

I used the --driver option you tell Minikube to use podman to create the Kubernetes cluster in. 
I also specified the container runtime, containerd. 

cri-o is recommended on the
[minikubes site](https://minikube.sigs.k8s.io/docs/drivers/podman/#usage) but on WSL2 it does not work well.

I started the container with the following command.

```
minikube start --driver=podman --container-runtime=containerd
```

At my first attempt I got an error about insufficient privileges, you will probably
see it too. Minikube explained very well what was wrong and also how to fix it.

```
😄  minikube v1.24.0 on Ubuntu 20.04 (amd64)
✨  Using the podman driver based on user configuration

💣  Exiting due to PROVIDER_PODMAN_NOT_RUNNING: "sudo -k -n podman version --format " exit status 1: sudo: a password is required
💡  Suggestion: Add your user to the 'sudoers' file: 'peza ALL=(ALL) NOPASSWD: /usr/bin/podman'
📘  Documentation: https://podman.io
```

I followed Minikube's advice and created a file in the /etc/sudoers.d directory with the suggested content. (You should replace *peza* with your user name.)

After I started a new bash shell, and tried again all was running well.

```
$ minikube start --driver=podman --container-runtime=containerd
😄  minikube v1.24.0 on Ubuntu 20.04 (amd64)
✨  Using the podman driver based on user configuration
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
E1212 17:58:23.097633   31696 cache.go:201] Error downloading kic artifacts:  not yet implemented, see issue #8426
🔥  Creating podman container (CPUs=2, Memory=2200MB) ...
📦  Preparing Kubernetes v1.22.3 on containerd 1.4.9 ...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔗  Configuring CNI (Container Networking Interface) ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

I tested the cluster with ```kubectl cluster-info``` and got the expected result.

```
$ kubectl cluster-info
Kubernetes control plane is running at https://127.0.0.1:45089
CoreDNS is running at https://127.0.0.1:45089/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

I also tested if I could successfully create a pod in the cluster.

```
$ kubectl run testpod -it --image=alpine
If you don't see a command prompt, try pressing enter.
/ #
```

As you can see, I was able to run alpine in an 
interactive shell in Minikube, which is handy for
 debugging pods and services on Kubernetes.


## Enabling traffic to minikube

Minikube has addons for ingress and load balancing that need to be enabled before use. 
But because minikube is running in a container, you need 
to perform an extra step to enable traffic from the host, the WSL2 environment, 
to the nodes in the cluster which runs in podman containers. 

The ```minikube tunnel``` command sets up a tunnel for each service of type loadbalancer from your WSL2 localhost to the minikube cluster running in the podman
container. 

At my first attempt at running the command, I got an error with the a lot of text with the reason for the error: 
it could not find the docker executable. 

Because I disabled Docker for Windows, I did not 
have the docker client installed. 

The docker client is listed at the [preparation](#preparation). 
If you already installed docker as part of the 
[preparation](#preparation), you will not see 
this error.

```
❌  Exiting due to DRV_PORT_FORWARD: get port 22 for "minikube": docker container inspect -f "'{{(index (index .NetworkSettings.Ports "22/tcp") 0).HostPort}}'" minikube: exec: "docker": executable file not found in $PATH
```

After I installed the docker client, I got another long error
message with the following line. 

```
stderr:
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
```

It turned out that the docker client expected a docker server to communicate with. 
But I was using podman. 

Luckily podman can act as a docker server. It can basically do anything Docker can, because the team that developed podman designed it to be a drop-in replacement for Docker.

Podman has the system service command that provides
an API that docker can talk to. 

```
$ podman system service --help
Run API service

Description:
  Run an API service

Enable a listening service for API access to podman commands.


Usage:
  podman system service [options] [URI]

Examples:
  podman system service --time=0 unix:///tmp/podman.sock

Options:
      --cors string   Set CORS Headers
  -t, --time int      Time until the service session expires in seconds.  Use 0 to disable the timeout (default 5)
  ```

To give docker the API it needs, I ran the command with --time=0 and the unix socket the docker client will look for, and left it running. Note that this command requires elevated privileges.

```
sudo -- podman system service \
    --time=0 \
    unix:///var/run/docker.sock &
```

Note: I used the *&* to put the command in the background. You can also run it in the foreground and continue working
 in another bash shell.

After that I needed to change the file mode, so minikube 
could access the socket.  

```
sudo -- chmod 777 \
    /var/run/docker.sock
```

I gave everyone access to the socket (not safe, but hé, this is Windows WSL2 and I amd the only user on the system).

Now I was able to run the minikube tunnel. This command also needed to keep running.

```
minikube tunnel
```

NOTE: You can stop the tunnel with Ctrl-C.

## Test Minikube with the metallb LoadBalancer

With the tunnel running, I was able to access a 
service of type loadbalancer in the cluster.

First I enabled the loadbalancer that comes with minikube, 
metallb.

```
minikube addons enable metallb
```

Then I created my standard service for experimentation.
It is an http server I wrote in Go that returns a 
page with information about the server and the http
headers. 

See the manifest below

```
apiVersion: v1
kind: Service
metadata:
  labels:
    app: myipaddress
  name: myipaddress
spec:
  ports:
  - port: 18080
    protocol: TCP
    targetPort: 8080
  selector:
    app: myipaddress
  type: LoadBalancer
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: myipaddress
  name: myipaddress
spec:
  containers:
  - image: docker.io/peterzandbergen/myipaddress
    name: myipaddress
```

I used curl to test the service and got a good response.

```
$ curl http://localhost:18080
Instance info:
    Instance:         myipaddress_1
    Server hostname:  myipaddress
    Open connections: 1
Request info:
    Host:                localhost:18080
    Method:              GET
    Request uri:         /
    Listen port:         8080
    Remote address:      10.244.0.1:15842
    UserAgent:           curl/7.68.0
Headers:
    Accept: */*
    User-Agent: curl/7.68.0
Body: size=0
```

## "minikube tunnel" and external ip address

You can experiment with the ```minikube tunnel``` and see that if you stop it, the LoadBalancer type service will not get an external IP address.

**With running tunnel**

```
$ kubectl get svc -n myipaddress-lb
NAME          TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)           AGE
myipaddress   LoadBalancer   10.101.47.2   127.0.0.1     18080:30649/TCP   2m56s
```

**Without running tunnel**

```
kubectl get svc -n myipaddress-lb
NAME          TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)           AGE
myipaddress   LoadBalancer   10.101.47.2   <pending>     18080:30649/TCP   4m26s
```


## Conclusion

Using some tricks and the right tools make it possible
to run run Kubernetes on WSL2
 and still have enough resources to leave your 
 Windows applications running next to it.

With this setup that runs Windows programs, Linux, Containers and Kubernetes, I can develop using Visual Studio Code running in Windows, target Linux in WSL2, build containers in podman and develop and run Kubernetes applications. 

And do not run out of memory. 

