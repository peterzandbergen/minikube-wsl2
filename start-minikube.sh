#!/bin/bash

CMD="minikube start --driver=podman --container-runtime=containerd $*"
# CMD="minikube start --driver=podman --container-runtime=cri-o $*"

echo CMD=$CMD
$CMD
